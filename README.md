
# hierr

原来的名字为`hun_error`.  包装OS的错误码, 统一错误码的查询和获取接口

错误码有两种使用场景:
1. 需要区分具体错误码: 比如底层`target`提供接口，需要调用者显示区分`target`.
2. 无需区分具体错误码: 一般有高层次，粗粒度的容错处理机制，错误码用于日志输出.
对于后一种场景，调用者决定日志输出方式，因此接口的返回值也多使用`Result<T, Error>`.
这里就会存在`Error`具体取值的构造尽可能和`Target`无关，因此为此场景定义了几个常用业务下需要使用到的错误码(`v0.2.4`):
1. Error::inval
1. Error::noent
1. Error::perm
1. Error::nomem
1. Error::busy
1. Error::timedout
1. Error::general
借用`Target`定义的错误码来表达业务层的常见错误.
因为此场景并不鼓励基于具体错误码进行容错处理，因此并不提供对应的`is_***`接口.

## 版本更新说明

1. `0.2.4`版本: 针对无需细分错误码场景提供常见错误构造函数
1. `0.2.3`版本: 支持mingw(target_os = "windows", target_env = "gnu"), 同linux.
1. `0.2.2`版本: 解决widows下无法正确输出中文信息的bug

## 接口和使用样例

```rust
1. fn errno() -> i32;
2. fn set_errno(i32);
3. fn errmsg(i32, &mut [u8]) -> &str;
```

封装i32为Error

# Example
```rust
use hierr;

let err = hierr::Error::last();
println!("{}", err);

let mut buf = [0_u8; 64];
println!("{}: {}",  hierr::errno(), hierr::errmsg(hierr::errno(), &mut buf[..]));

hierr::set_errno(100);
let err = hierr::Error::last();
assert_eq!(err, 100.into());
assert_eq!(hierr::errno(), 100);
```
