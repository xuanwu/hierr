use super::format_message_utf16;
use core::ptr;
use core::str;

#[link(name = "kernel32")]
extern "C" {
    fn GetLastError() -> usize;
    fn SetLastError(errno: usize);
    fn FormatMessageW(
        flags: usize,
        source: *const u8,
        err: usize,
        language: usize,
        buffer: *mut u16,
        size: usize,
        args: *const u8,
    ) -> usize;
}
pub fn errno() -> i32 {
    unsafe { GetLastError() as i32 }
}

pub fn set_errno(errno: i32) {
    unsafe { SetLastError(errno as usize) };
}

pub fn errmsg(errno: i32, buf: &mut [u8]) -> &str {
    const FORMAT_MESSAGE_IGNORE_INSERTS: usize = 0x200;
    const FORMAT_MESSAGE_FROM_SYSTEM: usize = 0x1000;
    const FORMAT_MESSAGE_MAX_WIDTH_MASK: usize = 0xFF;
    const FORMAT_MESSAGE_MASK: usize =
        FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_MAX_WIDTH_MASK;

    // 需要根据环境自动选择语言, 利用FormatMessageW而f非FormatMessageA统一Rust侧的处理
    // 下面固定为英文输出是原来利用FormatMessageA输出时，中文信息难以正确处理的规避方案, 不再需要
    // const LANG_ENGLISH: usize = 0x09;
    // const SUBLANG_ENGLISH_US: usize = 0x01;
    // const LANG_ID: usize = LANG_ENGLISH | (SUBLANG_ENGLISH_US << 10);

    let mut info = [0_u16; 64];
    let len = unsafe {
        FormatMessageW(
            FORMAT_MESSAGE_MASK,
            ptr::null(),
            errno as usize,
            0,
            info.as_mut_ptr(),
            info.len(),
            ptr::null(),
        )
    };
    return format_message_utf16(&info[..len], buf);
}

pub const WSA_INVALID_HANDLE: i32 = 6;
pub const WSA_NOT_ENOUGH_MEMORY: i32 = 8;
pub const WSA_INVALID_PARAMETER: i32 = 87;
pub const WSA_OPERATION_ABORTED: i32 = 995;
pub const WSA_IO_INCOMPLETE: i32 = 996;
pub const WSA_IO_PENDING: i32 = 997;
pub const WSAEINTR: i32 = 10004;
pub const WSAEBADF: i32 = 10009;
pub const WSAEACCES: i32 = 10013;
pub const WSAEFAULT: i32 = 10014;
pub const WSAEINVAL: i32 = 10022;
pub const WSAEMFILE: i32 = 10024;
pub const WSAEWOULDBLOCK: i32 = 10035;
pub const WSAEINPROGRESS: i32 = 10036;
pub const WSAEALREADY: i32 = 10037;
pub const WSAENOTSOCK: i32 = 10038;
pub const WSAEDESTADDRREQ: i32 = 10039;
pub const WSAEMSGSIZE: i32 = 10040;
pub const WSAEPROTOTYPE: i32 = 10041;
pub const WSAENOPROTOOPT: i32 = 10042;
pub const WSAEPROTONOSUPPORT: i32 = 10043;
pub const WSAESOCKTNOSUPPORT: i32 = 10044;
pub const WSAEOPNOTSUPP: i32 = 10045;
pub const WSAEPFNOSUPPORT: i32 = 10046;
pub const WSAEAFNOSUPPORT: i32 = 10047;
pub const WSAEADDRINUSE: i32 = 10048;
pub const WSAEADDRNOTAVAIL: i32 = 10049;
pub const WSAENETDOWN: i32 = 10050;
pub const WSAENETUNREACH: i32 = 10051;
pub const WSAENETRESET: i32 = 10052;
pub const WSAECONNABORTED: i32 = 10053;
pub const WSAECONNRESET: i32 = 10054;
pub const WSAENOBUFS: i32 = 10055;
pub const WSAEISCONN: i32 = 10056;
pub const WSAENOTCONN: i32 = 10057;
pub const WSAESHUTDOWN: i32 = 10058;
pub const WSAETOOMANYREFS: i32 = 10059;
pub const WSAETIMEDOUT: i32 = 10060;
pub const WSAECONNREFUSED: i32 = 10061;
pub const WSAELOOP: i32 = 10062;
pub const WSAENAMETOOLONG: i32 = 10063;
pub const WSAEHOSTDOWN: i32 = 10064;
pub const WSAEHOSTUNREACH: i32 = 10065;
pub const WSAENOTEMPTY: i32 = 10066;
pub const WSAEPROCLIM: i32 = 10067;
pub const WSAEUSERS: i32 = 10068;
pub const WSAEDQUOT: i32 = 10069;
pub const WSAESTALE: i32 = 10070;
pub const WSAEREMOTE: i32 = 10071;
pub const WSASYSNOTREADY: i32 = 10091;
pub const WSAVERNOTSUPPORTED: i32 = 10092;
pub const WSANOTINITIALISED: i32 = 10093;
pub const WSAEDISCON: i32 = 10101;
pub const WSAENOMORE: i32 = 10102;
pub const WSAECANCELLED: i32 = 10103;
pub const WSAEINVALIDPROCTABLE: i32 = 10104;
pub const WSAEINVALIDPROVIDER: i32 = 10105;
pub const WSAEPROVIDERFAILEDINIT: i32 = 10106;
pub const WSASYSCALLFAILURE: i32 = 10107;
pub const WSASERVICE_NOT_FOUND: i32 = 10108;
pub const WSATYPE_NOT_FOUND: i32 = 10109;
pub const WSA_E_NO_MORE: i32 = 10110;
pub const WSA_E_CANCELLED: i32 = 10111;
pub const WSAEREFUSED: i32 = 10112;
pub const WSAHOST_NOT_FOUND: i32 = 11001;
pub const WSATRY_AGAIN: i32 = 11002;
pub const WSANO_RECOVERY: i32 = 11003;
pub const WSANO_DATA: i32 = 11004;
pub const WSA_QOS_RECEIVERS: i32 = 11005;
pub const WSA_QOS_SENDERS: i32 = 11006;
pub const WSA_QOS_NO_SENDERS: i32 = 11007;
pub const WSA_QOS_NO_RECEIVERS: i32 = 11008;
pub const WSA_QOS_REQUEST_CONFIRMED: i32 = 11009;
pub const WSA_QOS_ADMISSION_FAILURE: i32 = 11010;
pub const WSA_QOS_POLICY_FAILURE: i32 = 11011;
pub const WSA_QOS_BAD_STYLE: i32 = 11012;
pub const WSA_QOS_BAD_OBJECT: i32 = 11013;
pub const WSA_QOS_TRAFFIC_CTRL_ERROR: i32 = 11014;
pub const WSA_QOS_GENERIC_ERROR: i32 = 11015;
pub const WSA_QOS_ESERVICETYPE: i32 = 11016;
pub const WSA_QOS_EFLOWSPEC: i32 = 11017;
pub const WSA_QOS_EPROVSPECBUF: i32 = 11018;
pub const WSA_QOS_EFILTERSTYLE: i32 = 11019;
pub const WSA_QOS_EFILTERTYPE: i32 = 11020;
pub const WSA_QOS_EFILTERCOUNT: i32 = 11021;
pub const WSA_QOS_EOBJLENGTH: i32 = 11022;
pub const WSA_QOS_EFLOWCOUNT: i32 = 11023;
pub const WSA_QOS_EUNKOWNPSOBJ: i32 = 11024;
pub const WSA_QOS_EPOLICYOBJ: i32 = 11025;
pub const WSA_QOS_EFLOWDESC: i32 = 11026;
pub const WSA_QOS_EPSFLOWSPEC: i32 = 11027;
pub const WSA_QOS_EPSFILTERSPEC: i32 = 11028;
pub const WSA_QOS_ESDMODEOBJ: i32 = 11029;
pub const WSA_QOS_ESHAPERATEOBJ: i32 = 11030;
pub const WSA_QOS_RESERVED_PETYPE: i32 = 11031;
