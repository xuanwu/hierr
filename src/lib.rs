//! 包装OS的错误码, 统一错误码的查询和获取接口
//! 1. fn errno() -> i32;
//! 2. fn set_errno(i32);
//! 3. fn errmsg(i32, &mut [u8]) -> &str;
//!
//! 封装i32为Error
//!
//! # Example
//! ```rust
//! use hierr;
//!
//! hierr::set_errno(100);
//! let err = hierr::Error::last();
//!
//! println!("{}", err);
//!
//! let mut buf = [0_u8; 64];
//! println!("{}: {}",  hierr::errno(), hierr::errmsg(hierr::errno(), &mut buf[..]));
//!
//! assert_eq!(err, 100.into());
//! assert_eq!(hierr::errno(), 100);
//!
//! ```

#![no_std]

use core::fmt;
use core::result;
use core::str;

pub type Result<T> = result::Result<T, Error>;

/// 错误码有两种使用场景:
/// 1. 需要区分具体错误码: 比如底层`target`提供接口，需要调用者显示区分`target`
/// 2. 无需区分具体错误码: 一般有高层次，粗粒度的容错处理机制，错误码用于日志输出.
/// 对于后一种场景，调用者决定日志输出方式，因此接口的返回值也多使用`Result<T, Error>`.
/// 这里就会存在`Error`具体取值的构造尽可能和`Target`无关，因此为此场景定义了几个常用业务下需要使用到的错误码:
/// 1. Error::inval
/// 1. Error::noent
/// 1. Error::perm
/// 1. Error::nomem
/// 1. Error::busy
/// 1. Error::timedout
/// 1. Error::general
/// 借用`Target`定义的错误码来表达业务层的常见错误.
/// 因为此场景并不鼓励基于具体错误码进行容错处理，因此并不提供对应的`is_***`接口.
#[derive(Copy, Clone, Eq, Ord, PartialEq, PartialOrd)]
#[repr(transparent)]
pub struct Error {
    pub errno: i32,
}

impl Error {
    pub const fn new(errno: i32) -> Self {
        Self { errno }
    }
    pub fn last() -> Self {
        Self { errno: errno() }
    }

    /// 表示输入错误. 适用于业务层主动返回错误码，避免使用具体数字导致跨平台兼容问题.
    pub const fn inval() -> Self {
        #[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
        {
            Self { errno: EINVAL }
        }
        #[cfg(all(target_os = "windows", not(target_env = "gnu")))]
        {
            Self {
                errno: WSA_INVALID_PARAMETER,
            }
        }
    }

    /// 表示资源不存在. 适用于业务层主动返回错误码，避免使用具体数字导致跨平台兼容问题.
    pub const fn noent() -> Self {
        #[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
        {
            Self { errno: ENOENT }
        }
        #[cfg(all(target_os = "windows", not(target_env = "gnu")))]
        {
            // ERROR_FILE_NOT_FOUND
            Self { errno: 1 }
        }
    }

    /// 表示权限不足. 适用于业务层主动返回错误码，避免使用具体数字导致跨平台兼容问题.
    pub const fn perm() -> Self {
        #[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
        {
            Self { errno: EPERM }
        }
        #[cfg(all(target_os = "windows", not(target_env = "gnu")))]
        {
            // ERROR_ACCESS_DENIED
            Self { errno: 5 }
        }
    }

    /// 表示内存不足. 适用于业务层主动返回错误码，避免使用具体数字导致跨平台兼容问题.
    pub const fn nomem() -> Self {
        #[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
        {
            Self { errno: ENOMEM }
        }
        #[cfg(all(target_os = "windows", not(target_env = "gnu")))]
        {
            Self {
                errno: WSA_NOT_ENOUGH_MEMORY,
            }
        }
    }

    /// 表示繁忙(内部资源受限)返回. 适用于业务层主动返回错误码，避免使用具体数字导致跨平台兼容问题.
    pub const fn busy() -> Self {
        #[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
        {
            Self { errno: EBUSY }
        }
        #[cfg(all(target_os = "windows", not(target_env = "gnu")))]
        {
            // ERROR_BUSY
            Self { errno: 170 }
        }
    }

    /// 表示超时返回. 适用于业务层主动返回错误码，避免使用具体数字导致跨平台兼容问题.
    pub const fn timedout() -> Self {
        #[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
        {
            Self { errno: ETIMEDOUT }
        }
        #[cfg(all(target_os = "windows", not(target_env = "gnu")))]
        {
            Self {
                errno: WSAETIMEDOUT,
            }
        }
    }

    /// 表示难以归类的通用错误. 适用于业务层主动返回错误码，避免使用具体数字导致跨平台兼容问题.
    pub const fn general() -> Self {
        Self { errno: -1 }
    }
}

impl Default for Error {
    fn default() -> Self {
        Error::general()
    }
}

impl From<i32> for Error {
    fn from(i32: i32) -> Self {
        Self::new(i32)
    }
}

impl From<Error> for Result<()> {
    fn from(err: Error) -> Self {
        if err.errno == 0 {
            Ok(())
        } else {
            Err(err)
        }
    }
}

const ERRMSG_MAX_SIZE: usize = 64;
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> result::Result<(), fmt::Error> {
        let mut msg = [0_u8; ERRMSG_MAX_SIZE];
        write!(f, "{}: {}", self.errno, errmsg(self.errno, &mut msg[..]))
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> result::Result<(), fmt::Error> {
        let mut msg = [0_u8; ERRMSG_MAX_SIZE];
        write!(f, "{}: {}", self.errno, errmsg(self.errno, &mut msg[..]))
    }
}

#[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
fn format_message_utf8<'a>(input: &[u8], output: &'a mut [u8]) -> &'a str {
    let Ok(s) = str::from_utf8(input) else {
        return get_message(output, 0);
    };
    if input.len() <= output.len() {
        output[..input.len()].copy_from_slice(input);
        return str::from_utf8(&output[..input.len()]).unwrap();
    }
    return truncate_message(s, output);
}

#[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
fn truncate_message<'a>(input: &str, output: &'a mut [u8]) -> &'a str {
    let mut pos = 0;
    for c in input.chars() {
        if !put_message(output, &mut pos, c) {
            break;
        }
    }
    get_message(output, pos)
}

fn put_message(output: &mut [u8], pos: &mut usize, c: char) -> bool {
    if *pos + c.len_utf8() <= output.len() {
        c.encode_utf8(&mut output[*pos..]);
        *pos += c.len_utf8();
        return true;
    }
    false
}

fn get_message(output: &[u8], len: usize) -> &str {
    if len > 0 {
        str::from_utf8(&output[..len]).unwrap()
    } else {
        "Unknown error"
    }
}

#[cfg(all(target_os = "windows", not(target_env = "gnu")))]
fn format_message_utf16<'a>(input: &[u16], output: &'a mut [u8]) -> &'a str {
    let mut pos = 0;
    char::decode_utf16(input.iter().copied()).any(|c| {
        let Ok(c) = c else {
            pos = 0;
            return true;
        };
        !put_message(output, &mut pos, c)
    });
    get_message(output, pos)
}

#[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
mod unix;
#[cfg(any(unix, all(target_os = "windows", target_env = "gnu")))]
pub use unix::*;

#[cfg(all(target_os = "windows", not(target_env = "gnu")))]
mod windows;
#[cfg(all(target_os = "windows", not(target_env = "gnu")))]
pub use windows::*;

#[cfg(test)]
mod test {
    use super::*;
    extern crate std;
    use std::println;

    #[test]
    fn test_errno() {
        set_errno(100);
        assert_eq!(errno(), 100);
    }

    #[test]
    fn test_print() {
        for n in 0..=102 {
            let err = Error::new(n);
            println!("[{}]\n[{:?}]\n", err, err);
        }

        set_errno(4);
        println!("5: {}", errmsg(5, &mut [0_u8; 7]));
    }
}
